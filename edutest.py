import re
import ast
import sys
import runpy
import types
import locale
import inspect
import importlib
import os.path
from copy import deepcopy

import threading
from traceback import print_exc
try:
    import thread
except ImportError:
    import _thread as thread


OUT_OF_INPUTS="Program asked for input but no more input was available."
PROGRAM_SHOULDNT_ASK_INPUT="Program asked for input but shouldn't have done this."
INPUT_NOT_ALLOWED="Reading input is not allowed"
OUTPUT_NOT_ALLOWED="Writing output is not allowed"
REMAINING_INPUTS = "Program didn't use all inputs provided by the test. Unused inputs: {remaining}"
CANT_FIND_FUNCTION="Can't find function {name}"
CANT_FIND_FILE="Can't find file {path}"
CANT_IMPORT_MODULE_WITH_INPUT="Can't import {name} as module, because it tried to read input"
FUNCTION_MODIFIED_ARGUMENTS="Your function modified its argument objects: {}"
FUNCTION_MODIFIED_GLOBALS="Your function modified following global variables or corresponding objects: {}"
FUNCTION_PRINTED_INSTEAD_OF_RETURNING="Function printed correct response but should have returned it"
FUNCTION_RETURNED_WRONG_TYPE="Function returned a value of type {actual_type} ({actual_result}), but should have returned a value of type {expected_type} ({expected_result})"
FUNCTION_DIDNT_RETURN="Function didn't return anything (returned None). Do you have correct return for all branches?"
FUNCTION_DIDNT_RETURN_BUT_PRINTED="Function didn't return anything (returned None). Did you confuse print for return?"
FUNCTION_RETURNED_WRONG_VALUE="Function returned {actual}, but should have returned {expected}"
FUNCTION_READ_STDIN="Function tried to read input"
FUNCTION_WROTE_STDOUT="Function printed something but this was not allowed"
FUNCTION_WROTE_STDERR="Function wrote to error stream but this was not allowed"

STDERR="error stream"
LAST_STDOUT="output after last input"
STDOUT="output"
PROGRAM_USAGE_LOOKED_LIKE="Program usage was following:\n{}"
EXPECTED_STREAM_TO_BE="Expected program's {} to be \n{}"
EXPECTED_STREAM_TO_STAY_EMPTY="Expected program's {} to stay empty."
STREAM_ALMOST_BUT_TRAILING="Program's {} was almost correct, difference was in trailing whitespace."
STREAM_ALMOST_BUT_CASE_OR_WHITESPACE="Program's {} was almost correct, difference was in whitespace and/or the case of letters."
EXPECTED_NUMBERS="Expected program's {} to contain following numbers (in this order): {}"


ALLOW_DECIMAL_COMMA = False

class _ModificationChecker:
    def __init__(self, items):
        assert isinstance(items, dict)
        
        # some items can't be deepcopied
        self.items = {}
        for key in items:
            try:
                deepcopy(items[key])
                self.items[key] = items[key]
            except:
                pass
        
        self.copy = deepcopy(self.items)
    
    def get_modified_items(self):
        if self.items == self.copy:
            return []
        elif len(self.items) != len(self.copy):
            return ["__len__"]
        else:
            mods = []
            for key in self._get_copy_keys():
                if (not self._items_has_key(key)
                    or self.copy[key] != self.items[key]): 
                    mods.append(key)
            
            return mods
            
    def _get_copy_keys(self):
        if isinstance(self.copy, dict):
            return self.copy.keys()
        elif isinstance(self.copy, (list, tuple)):
            return list(range(len(self.copy)))
        else:
            raise Exception("Unsupported type")
    
    def _items_has_key(self, key):
        if isinstance(self.items, dict):
            return key in self.items
        elif isinstance(self.items, (list, tuple)):
            return 0 <= key < len(self.items)
        else:
            raise Exception("Unsupported type")
    

class Arguments:
    """Collection of positional and named arguments to be used with :func:`check_function` and :func:`generate_function_test`.
    
    Example::
     
        check_function("fun1", Arguments(3, 4, kind="mult"), 12)
    """
    def __init__(self, *positional, **named):
        self.positional = positional
        self.named = named
    
    def __str__(self):
        items = map(repr, self.positional)
        for key in self.named:
            items.append(repr(key) + "=" + repr(self.named[key]))
        return "(" + ", ".join(items) + ")"
    
    def __repr__(self):
        return self.__class__.__name__ + str(self)



def find_function(name, scope=None, import_inputs=[], import_files={}):
    """Tries to locate given function, importing when necessary.
    
    :param name: String containing module name and function name, eg. ``"mod1.fun1"``. 
        Can be just function name if scope is given with ``scope`` argument.
    
    :param scope: Either name of a module or dictionary of functions.
    
    :param import_inputs: If the module containing the function needs to read
        `stdin` while being imported, then use this argument to provide inputs.
        See :func:`import_module` for more details.
    
    :param import_files: If the module needs to access files 
        while being imported, then use this argument to provide temporary files.
        See :func:`import_module` for more details.
    
    :return: the function object
    
    :raises AssertionError: if the function can't be found
    
    Examples::
        
        f1 = find_function("mod1.fun1")
        f2 = find_function("fun2", "mod1")
        f3 = find_function("fun3", vars(mod1)) 
    """
    assert isinstance(name, str)
    
    if scope is None:
        assert "." in name
        scope, name = name.rsplit(".", maxsplit=1)
    
    if isinstance(scope, str):
        scope = import_module(scope, inputs=import_inputs, temp_files=import_files)
    
    if isinstance(scope, types.ModuleType):
        scope_dict = vars(scope)
    else:
        assert isinstance(scope, dict)
        scope_dict = scope
    
    # TODO: format with qualified name
    assert name in scope_dict, CANT_FIND_FUNCTION.format(name=name)
    assert callable(scope_dict[name]), str(name) + " is not callable"
    return scope_dict[name]

def import_module(name, package=None, inputs=[], temp_files={},
                  text_files_encoding="UTF-8"):
    """Works like :func:`importlib.import_module` but can provide input and hides output.
    
    
    """
    # TODO: create temp-file context manager
    for path in temp_files:
        create_file(path, temp_files[path], text_files_encoding)
        
    try:
        with IOCapturing(inputs):
            return importlib.import_module(name, package)
    finally:
        for path in temp_files:
            try:
                os.remove(path)
            except:
                pass
            

def find_changed_variables(before, after, check_id):
    
    changed_vars = set()
    for name in before:
        if (name not in after
            or before[name] != after[name]
            or check_id and id(before[name]) != id(after[name])):
            changed_vars.add(name)
    for name in after:
        if name not in before:
            changed_vars.add(name)
    
    return changed_vars

def check_function(function, arguments, expected_result,
                   inputs=[],
                   provided_files={},
                   import_inputs=[],
                   import_files={},
                   text_files_encoding="UTF-8",
                   allow_remaining_inputs=False,
                   expected_stdout="",
                   expected_last_stdout=None,
                   expected_stderr="",
                   allow_modification_of_globals=False,
                   allow_modification_of_arguments=False,
                   timeout=None):
    
    if isinstance(function, str):
        function = find_function(function,
                                 import_inputs=import_inputs,
                                 import_files=import_files)
    assert callable(function), str(function) + " is not callable"
    positional_arguments, keyword_arguments = _extract_arguments(arguments)
    # prepare the call
    if not allow_modification_of_globals:
        try:
            module = inspect.getmodule(function)
        except Exception as e:
            raise RuntimeError("Can't get function module") from e
        
        # TODO: check ID changes
        all_items = vars(module)
        # Some special items seem to change automatically
        relevant_items = {name : all_items[name] for name in all_items if not name.startswith("__")}
        globals_checker = _ModificationChecker(relevant_items)
    
    if not allow_modification_of_arguments:
        args_checker = _ModificationChecker(_combine_arguments(positional_arguments,
                                                                  keyword_arguments))
    
    # make the call
    with IOCapturing(inputs) as iocap:
        try:
            for path in provided_files:
                create_file(path, provided_files[path], text_files_encoding)
                
            if timeout:
                with Timeout(timeout):
                    actual_result = function(*arguments, **keyword_arguments)
            else:
                actual_result = function(*arguments, **keyword_arguments)
                
        except OutOfInputsError:
            if inputs in ["", [], ()]:
                raise ForbiddenInputError(FUNCTION_READ_STDIN)
            else:
                raise
        
        finally:
            for path in provided_files:
                try:
                    delete_file(path)
                except:
                    pass # TODO: ???
            
    stdout = iocap.get_stdout()
    stderr = iocap.get_stderr()
    
    # check result
    if callable(expected_result):
        expected_result(actual_result)
    elif actual_result != expected_result:
        
        # Maybe student used print instead of return?
        if (actual_result == None 
            and str(expected_result).strip() == stdout.strip()): 
            raise AssertionError(FUNCTION_PRINTED_INSTEAD_OF_RETURNING)
        
        if not isinstance(actual_result, type(expected_result)):
            if actual_result is None:
                if stdout != "":
                    raise AssertionError(FUNCTION_DIDNT_RETURN_BUT_PRINTED)
                else:
                    # TODO: erista ilma returnita funktsioonid neist, kus mõnes harus
                    # return puudu või tagastatakse valet asja
                    raise AssertionError(FUNCTION_DIDNT_RETURN)
            else:
                raise AssertionError(FUNCTION_RETURNED_WRONG_TYPE.format(
                                     expected_type=type(expected_result).__name__,
                                     actual_type=type(actual_result).__name__,
                                     expected_result=repr(expected_result),
                                     actual_result=repr(actual_result)))
                
        raise AssertionError(FUNCTION_RETURNED_WRONG_VALUE.format(
                                 expected=repr(expected_result),
                                 actual=repr(actual_result)))
    
    if expected_stdout == "" and stdout != "":
        raise AssertionError(FUNCTION_WROTE_STDOUT)
    elif expected_stderr == "" and stderr != "":
        raise AssertionError(FUNCTION_WROTE_STDERR)
    else:
        check_std_streams(iocap, expected_stdout, expected_last_stdout, expected_stderr, allow_remaining_inputs)
          
    
    # check other conditions
    if not allow_modification_of_arguments:
        changed_args = args_checker.get_modified_items()
        if len(changed_args) > 0:
            raise AssertionError(FUNCTION_MODIFIED_ARGUMENTS
                                 .format(", ".join(sorted(map(str,changed_args)))))
    
    if not allow_modification_of_globals:
        changed_globals = globals_checker.get_modified_items()
        if len(changed_globals) > 0:
            raise AssertionError(FUNCTION_MODIFIED_GLOBALS
                                 .format(", ".join(sorted(map(str,changed_globals)))))
         

def check_script(script_path, inputs=[], arguments=[],
                 provided_files={},
                 allow_remaining_inputs=False,
                 expected_stdout=None, 
                 expected_last_stdout=None,
                 expected_stderr="",
                 expected_files={},
                 text_files_encoding="UTF-8",
                 text_files_universal_newlines=True,
                 ):
    try:
        # preparations
        for path in provided_files:
            create_file(path, provided_files[path], text_files_encoding)
        
        result = run_script(script_path, inputs, arguments)
        check_std_streams(result, expected_stdout, expected_last_stdout, expected_stderr, allow_remaining_inputs)
        
        for path in expected_files:
            check_file_contains(path, expected_files[path],
                                text_files_encoding, text_files_universal_newlines) 
        
        return result
    
    finally:
        # remove temp files
        # TODO: detect unclosed files and close them
        for path in set(provided_files.keys()).union(expected_files.keys()):
            if os.path.exists(path):
                # TODO: provide specific error message when file was left open
                try:
                    os.remove(path)
                except:
                    pass
                

def check_file_contains(path, expected_content, text_file_encoding="UTF-8",
                        universal_newlines=True):
    
    assert os.path.exists(path), CANT_FIND_FILE.format(path=path)
    
    if isinstance(expected_content, str):
        expected_content = expected_content
        with open(path, encoding=text_file_encoding) as fp:
            actual_content = fp.read()
        
        if universal_newlines:
            expected_content = expected_content.replace("\r\n", "\n")
            actual_content = actual_content.replace("\r\n", "\n")
        
        if actual_content != expected_content:
            raise AssertionError("TODO: file content doesn't match")
    
    elif isinstance(expected_content, bytes):
        with open(path, mode="b") as fp:
            actual_content = fp.read()
        
        if actual_content != expected_content:
            raise AssertionError("TODO: file content doesn't match")
    
    elif expected_content is None:
        pass
    else:
        raise TypeError("Unsupported expected_content")
        

def check_std_streams(iocap, expected_stdout=None, expected_last_stdout=None, expected_stderr="",
                      allow_remaining_inputs=False, ignore_trailing_whitespace=True):
    
    def essence(s):
        return ' '.join(s.split()).lower()
        
    
    # TODO: english
    for stream_title, expected, actual in [
        (STDERR, expected_stderr, iocap.get_stderr()),
        (LAST_STDOUT, expected_last_stdout, iocap.get_last_stdout()),
        (STDOUT, expected_stdout, iocap.get_stdout()),
        ]:
        
        io_description = PROGRAM_USAGE_LOOKED_LIKE.format(quote_text_block(iocap.get_io()))
        
        # TODO: what about \r\n vs \n ??? Does it work out always?
        if expected is None:
            pass
        elif isinstance(expected, list):
            actual_numbers = extract_numbers(actual)
            assert actual_numbers == expected,\
                EXPECTED_NUMBERS.format(stream_title, expected) + " " + io_description
        elif callable(expected):
            # TODO: check with predicate
            raise NotImplemented()
        elif expected != actual: 
            if expected == "":
                raise AssertionError(EXPECTED_STREAM_TO_STAY_EMPTY
                                     .format(stream_title) + " " + io_description)
                
            else:
                difference_description = (EXPECTED_STREAM_TO_BE
                        .format(stream_title, quote_text_block(expected)) + "\n\n" +  io_description)
                 
                if expected.rstrip() == actual.rstrip():
                    assert ignore_trailing_whitespace, (
                    STREAM_ALMOST_BUT_TRAILING
                    .format(stream_title) + " " + difference_description)
                     
                elif essence(expected) == essence(actual):
                    raise AssertionError(
                        STREAM_ALMOST_BUT_CASE_OR_WHITESPACE
                        .format(stream_title) + " " + difference_description
                    )
                
                else:
                    raise AssertionError(difference_description)
        
        assert allow_remaining_inputs or len(iocap.get_remaining_inputs()) == 0, \
            REMAINING_INPUTS.format(remaining=iocap.get_remaining_inputs())
            

def check_code(obj,
               disallow_keywords=[],
               disallow_names=[]):
    "TODO:"
    # return(print(...))
    


def run_script(script_path, inputs=[], arguments=[]):
    """
    :rtype: :class:ScriptResult
    """
    original_argv = sys.argv
    sys.argv = [script_path] + arguments 

    globs = {}    
    try:
        with IOCapturing(inputs) as iocap:
            globs = runpy.run_path(script_path)
    finally:
        sys.argv = original_argv
    
    return ScriptResult(iocap, globs)

def generate_function_test(function, arguments, expected_result,
                           doc=None, **options):
    
    doc = _format_call(function, arguments) 
    
    def test():
        check_function(function, arguments, expected_result, **options)
    
    _register_test(test, doc) 

def generate_script_test(script_path, inputs=[], arguments=[], doc=None, **options):
    if doc is None:
        doc = script_path
        if len(inputs) > 0:
            doc += " " + str(inputs)
        if len(arguments) > 0:
            doc += " " + str(arguments)
    
    def test():
        check_script(script_path, inputs, arguments, **options)
    
    _register_test(test, doc)

def parameterize(argument_sets):
    
    def decorator(function):
        for arguments in argument_sets:
            generate_custom_test(function, arguments)
    
    return decorator

def generate_custom_test(function, arguments=(), doc=None):
    """Creates and registers a new test function based on existing function.
    
    :param arguments: Arguments to be passed to the target function. Use
        tuple or list for passing only positional arguments or Arguments object
        for passing positional and/or named arguments.
    
    :param doc: String to be used as docstring in generated test function.
        If None, then docstring will be taken from target function 
        TODO: explain more.
    
    :param timeout: if int or float, then test interrupts target function after
        this amount of seconds.
    """
    positional_arguments, named_arguments = _extract_arguments(arguments)
    
    if doc is None:
        try:
            doc = function.__doc__.format(*positional_arguments,
                                          **named_arguments)
        except:
            doc = _format_call(function, arguments)
    
    def test():
        function(*positional_arguments, **named_arguments)
        
    _register_test(test, doc)
        
class Timeout:
    """Context manager for running code with timeout.
    
    Only works if run in main thread.
    Idea taken from http://stackoverflow.com/a/31667005/261181
    """
    def __init__(self, timeout_in_seconds):
        self.timer = threading.Timer(timeout_in_seconds, thread.interrupt_main)
        
    def __enter__(self):
        self.timer.start()
        
    def __exit__(self, exc_type, exc_value, traceback):
        # TODO: massage interrupt exception ???
        self.timer.cancel()
    

class ForbiddenIO:
    """Context manager which causes access to given standard streams
    to raise :exc:`ForbiddenOutputError` or :exc:`ForbiddenInputError`
    
    
    Example ::
        
        with ForbiddenIO({"stdin", "stderr"}):
            print("Blah 2")                         # OK, stdout is not forbidden
            something = input("input something: ")  # raises ForbiddenInputError
            
    """
    def __init__(self, forbidden_streams={"stdin", "stdout", "stderr"}):
        self._forbidden_streams = forbidden_streams
        self._original_streams = {}
        
    def __enter__(self):
        for name in self._forbidden_streams:
            self._original_streams[name] = getattr(sys, name)
            setattr(sys, name, _ForbiddenStream())
        
    def __exit__(self, exc_type, exc_value, traceback):
        for name in self._forbidden_streams:
            setattr(sys, name, self._original_streams[name])

class ForbiddenInput(ForbiddenIO):
    def __init__(self):
        ForbiddenIO.__init__(self, {"stdin"})
        
class FakeInput:
    def __init__(self, inputs=[]):
        self._inputs=inputs
        self._original_stdin = None
    
    def __enter__(self):
        self._inputs_iterator = self._create_inputs_iterator(self._inputs)
        self._remaining_inputs = []
        self._original_stdin = sys.stdin
        
        # Install fake stream
        sys.stdin = _InputListenerStream(self._record_stdin_data, self._inputs_iterator)
        
        return self
    
    def _record_stdin_data(self, data):
        pass
    
    def __exit__(self, exc_type, exc_value, traceback):
        self._remaining_inputs = sys.stdin.fetch_remaining_inputs()
        
        # restore original stream
        sys.stdin = self._original_stdin
    
    def get_remaining_inputs(self):
        return self._remaining_inputs
                
    def _create_inputs_iterator(self, inputs):
        if isinstance(inputs, str):
            return iter(inputs.splitlines())
            
        try:
            return iter(inputs)
        except TypeError:
            try:
                result = inputs(self)
                assert isinstance(result, types.GeneratorType)
                return result
                
            except BaseException as e:
                e.message = "Could not create iterator for inputs.\n" + e.message
                raise

class IOCapturing:
    def __init__(self, inputs=[]):
        self._inputs=inputs
    
    
    def __enter__(self):
        self._inputs_iterator = self._create_inputs_iterator(self._inputs)
        self._stream_events = []
        self._original_streams = {}
        self._remaining_inputs = []

        # Remember original streams
        for stream_name in {"stdout", "stderr", "stdin"}:
            self._original_streams[stream_name] = getattr(sys, stream_name)
        
        # Install fake streams
        sys.stdout = _OutputListenerStream(self._record_stdout_data)
        sys.stderr = _OutputListenerStream(self._record_stderr_data)
        sys.stdin = _InputListenerStream(self._record_stdin_data, self._inputs_iterator)
        
        return self

    
    def __exit__(self, exc_type, exc_value, traceback):
        self._remaining_inputs = sys.stdin.fetch_remaining_inputs()
        
        # restore original streams
        for stream_name in {"stdout", "stderr", "stdin"}:
            setattr(sys, stream_name, self._original_streams[stream_name])
    
    
    def get_remaining_inputs(self):
        return self._remaining_inputs
                
    def get_io(self):
        return self._get_stream_data({"stdout", "stderr", "stdin"}, False)
        
    def get_stdout(self):
        return self._get_stream_data({"stdout"}, False)
    
    def get_last_stdout(self):
        return self._get_stream_data({"stdout"}, True)
    
    def get_stderr(self):
        return self._get_stream_data({"stderr"}, False)
    
    def debug(self, *args,  sep=' ', end='\n', stream_name="stdout", flush=False):
        """Meant for printing debug information from input generator."""
        print(*args, sep=sep, end=end, 
              file=self._original_streams[stream_name], flush=flush)
        
    def _record_stdout_data(self, data):
        self._stream_events.append(("stdout", data))
        
    def _record_stderr_data(self, data):
        self._stream_events.append(("stderr", data))
        
    def _record_stdin_data(self, data):
        self._stream_events.append(("stdin", data))
    
    def _get_stream_data(self, stream_names, only_since_last_input):
        result = ""
        i = len(self._stream_events)-1
        while i >= 0:
            event_stream_name, event_data = self._stream_events[i]
            
            if only_since_last_input and event_stream_name == "stdin":
                break
            
            if event_stream_name in stream_names:
                result = event_data + result
            
            i -= 1
        
        return result
    
    def _create_inputs_iterator(self, inputs):
        if isinstance(inputs, str):
            return iter(inputs.splitlines())
            
        try:
            return iter(inputs)
        except TypeError:
            try:
                result = inputs(self)
                assert isinstance(result, types.GeneratorType)
                return result
                
            except BaseException as e:
                e.message = "Could not create iterator for inputs.\n" + e.message
                raise
        

class ScriptResult:
    def __init__(self, iocap, globs):
        self._iocap = iocap
        self.globals = globs
    
    def get_remaining_inputs(self):
        return self._iocap.get_remaining_inputs()
                
    def get_io(self):
        return self._iocap.get_io()
        
    def get_stdout(self):
        return self._iocap.get_stdout()
    
    def get_last_stdout(self):
        return self._iocap.get_last_stdout()
    
    def get_stderr(self):
        return self._iocap.get_stderr()

class _ListenerStream:
    def __init__(self, data_listener):
        self._data_listener = data_listener
        

class _OutputListenerStream(_ListenerStream):
    def __init__(self, data_listener):
        _ListenerStream.__init__(self, data_listener)
         
    def write(self, data):
        self._data_listener(data)
    
    def writelines(self, lines):
        self._data_listener(lines)

class _InputListenerStream(_ListenerStream):
    
    def __init__(self, data_listener, inputs_iterator):
        _ListenerStream.__init__(self, data_listener)
        self._inputs_iterator = inputs_iterator
        self._consumed_inputs = []
    
    def fetch_remaining_inputs(self):
        result = []
        while True:
            try:
                result.append(next(self._inputs_iterator))
            except StopIteration:
                break
        
        return result
    
    def read(self, limit=-1):
        raise NotImplementedError("Method 'read' on stdin is not supported in testable programs")
    
    def readline(self, limit=-1):
        if limit > 0:
            raise NotImplementedError("Positive limit/size for stdin.readline is not supported in testable programs")
        
        try:
            data = str(next(self._inputs_iterator)) + "\n"
            if self._data_listener:
                self._data_listener(data)
            
            self._consumed_inputs.append(data)
            return data
        except StopIteration:
            if len(self._consumed_inputs) == 0:
                msg = PROGRAM_SHOULDNT_ASK_INPUT
            else:
                msg = OUT_OF_INPUTS
                
            raise OutOfInputsError(msg)
        
    def readlines(self, limit=-1):
        raise NotImplementedError("Method 'readlines' on stdin is not supported in testable programs")
        

class ForbiddenStreamError(AssertionError):
    pass

class OutOfInputsError(AssertionError):
    pass

class ForbiddenInputError(ForbiddenStreamError):
    pass

class ForbiddenOutputError(ForbiddenStreamError):
    pass


class _ForbiddenStream:
    def write(self, data):
        raise ForbiddenOutputError(OUTPUT_NOT_ALLOWED)
    
    def writelines(self, lines):
        raise ForbiddenOutputError(OUTPUT_NOT_ALLOWED)
    
    def read(self, limit=-1):
        raise ForbiddenInputError(INPUT_NOT_ALLOWED)
    
    def readline(self, limit=-1):
        raise ForbiddenInputError(INPUT_NOT_ALLOWED)
        
    def readlines(self, limit=-1):
        raise ForbiddenInputError(INPUT_NOT_ALLOWED)



def load_locale(locale):
    """Example: if argument is "et", then all global variables defined
    in edutest_locale_et.py are copied to edutest, overriding existing values"""
    try:
        this_module = sys.modules[__name__]
        that_module = importlib.import_module("edutest_locale_" + locale)
        
        for name in vars(that_module):
            if not name.startswith("_"):
                setattr(this_module, name, getattr(that_module, name))
    except:
        pass

def _register_test(f, doc=None):
    
    def _find_test_module():
        # Find the frame where filename changes
        # NB! This approach failes, if there is another module called between test module and edutest
        frame = inspect.currentframe()
        this_file = frame.f_code.co_filename
        
        while frame.f_code.co_filename == this_file:
            frame = frame.f_back
        
        return inspect.getmodule(frame)
    
    module = _find_test_module() 
    if doc:
        f.__doc__ = doc
    f.__test__ = True
    name = _generate_test_name(module, doc)
    f.__name__ = name
    setattr(module, name, f)
    
         
def _extract_arguments(arguments):
    if isinstance(arguments, Arguments):
        return arguments.positional, arguments.named
    elif isinstance(arguments, dict):
        return (), arguments
    else:
        return arguments, {}

def _combine_arguments(positional, keyword):
    result = {}
    for key in keyword:
        result[key] = keyword
    for i in range(len(positional)):
        result[i] = positional[i]
        
    return result    

def _format_call(function, arguments):
    return _format_function_name(function) + _format_arguments(arguments)
  
def _format_arguments(arguments):
    if isinstance(arguments, Arguments):
        return str(arguments)
    else:
        return "(" + ", ".join(map(repr, arguments)) + ")"

def _format_function_name(function):
    if isinstance(function, str):
        return function
    else:
        return function.__module__ + "." + function.__name__
    # TODO: try to be more adaptive here
    #return function.__name__

def _generate_test_name(module, doc):
    name = "test_" + doc.replace("\r\n", "_").replace("\n", "_")
    while hasattr(module, name):
        name += "_"
    return name
    
def get_source(path):
    import tokenize
    # encoding-safe open
    with tokenize.open(path) as sourceFile:
        contents = sourceFile.read()
    return contents

def is_function(value):
    try:
        return hasattr(value, '__call__')
    except:
        return False

def quote_text_block(text):
    return "\n>" + text.replace("\n", "\n>")

def extract_numbers(s, allow_decimal_comma=False):
    result = []
    
    if allow_decimal_comma:
        rexp = """((?:\+|\-)?\d+(?:(?:\.|,)\d+)?)"""
    else:
        rexp = """((?:\+|\-)?\d+(?:\.\d+)?)"""
        
    for item in re.findall(rexp, s):
        try:
            result.append(int(item))
        except:
            try:
                result.append(float(item.replace(",", ".")))
            except:
                pass
    return result

def contains_number(num_list_or_str, x, allowed_error=0, allow_decimal_comma=False):
    if isinstance(num_list_or_str, str):
        nums = extract_numbers(num_list_or_str, allow_decimal_comma)
    else:
        nums = num_list_or_str
    
    for num in nums:
        if abs(num - x) <= allowed_error:
            return True
    
    return False

def ast_contains_name(node, name):
    if isinstance(node, ast.Name) and node.id == name:
        return True
    
    for child in ast.iter_child_nodes(node):
        if ast_contains_name(child, name):
            return True

    return False

def ast_contains(node, node_type):
    if isinstance(node, node_type):
        return True
    
    for child in ast.iter_child_nodes(node):
        if ast_contains(child, node_type):
            return True

    return False

def create_file(path, content, text_files_encoding="UTF-8"):
    if text_files_encoding is None:
        text_files_encoding = locale.getpreferredencoding(False)
    
    if isinstance(content, str):
        content = content.encode(text_files_encoding)
        
    with open(path, mode="wb") as fp:
        fp.write(content)

def delete_file(path):
    " TODO: http://stackoverflow.com/questions/40315334/how-to-find-an-open-file-handle-by-path-in-python"

def _hide_from_nose():
    # hide all functions from nose (there are several with "test" in name)
    for name in vars(sys.modules[__name__]).copy():
        if "test" in name.lower():
            try:
                setattr(getattr(sys.modules[__name__], name), "__test__", False)
            except:
                pass

def _load_preferred_locale():
    def get_language_code_from_locale_string(s):
        return s.split(".")[0].split("_")[0]

    if "VPL_LANG" in os.environ: # locale set by VPL server (http://vpl.dis.ulpgc.es/)
        load_locale(get_language_code_from_locale_string(os.environ["VPL_LANG"]))
    else:
        try:
            load_locale(get_language_code_from_locale_string(locale.getdefaultlocale()[0]))
        except Exception as e:
            print(e)
            return
    

_load_preferred_locale()
_hide_from_nose()
