.. edutest documentation master file, created by
   sphinx-quickstart on Tue Dec  8 15:23:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========================================
Checking students' code with ``edutest``
========================================

``edutest`` is a Python utility library for writing tests in the context of teaching programming. It can be used in any testing framework.




.. note::

    If you need to check students' work in `VPL <http://vpl.dis.ulpgc.es/>`_, then you may want to use ``edutest`` together with `vpltest <https://bitbucket.org/plas/vpltest>`_.
      

Contents:

.. toctree::
    :maxdepth: 2
    
    getting  
    usage 
    edutest



