Usage examples
==============

First example
-------------
Let's say you give your students following task:

    Write a program which asks user to input a temperature in Celsius scale and outputs it
    in Fahrenheit scale.
    
The testing script could be

.. sourcecode:: py3

    def test1():
        check_script("demos/cel_far.py", [38],
                     expected_last_stdout=[100.4])    

    
Running the tests
-----------------
