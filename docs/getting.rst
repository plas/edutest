Getting ``edutest``
===================

Just do 

.. sourcecode:: bash

    pip install edutest

NB! If you have several Python versions installed then make sure you're using correct pip.


.. note::

    If you can't or don't want to use pip, then download zip file from https://pypi.python.org/pypi/edutest and extract ``edutest.py`` somewhere in your Python path.
    
    If you need latest source, then go to https://bitbucket.org/plas/edutest.
