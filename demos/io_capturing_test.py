from runpy import run_path
from edutest import IOCapturing, extract_numbers



def check(a, b, expected_sum, expected_product):
    with IOCapturing([a, b]) as cap:
        run_path("io_capturing_solution.py")
    
    assert cap.get_remaining_inputs() == []
    nums = extract_numbers(cap.get_last_stdout())
    assert nums == [expected_sum, expected_product]
    

def test1():
    check(3,4, 7, 12)
    
def test2():
    check(3,0, 3, 0)


if __name__ == "__main__":
    test1()
    test2()