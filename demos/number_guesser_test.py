from runpy import run_path
from edutest import extract_numbers
from edutest.capturing import IOCapturing

def do_test(target):
    
    def generate_responses(cap):
        guess_count = 0
        while True:
            numbers = extract_numbers(cap.get_last_stdout())
            guess_count += 1
            assert len(numbers) == 1
            offer = numbers[0]
            if target < offer:
                yield "s"
            elif target > offer:
                yield "b"
            else:
                yield "c"
                break
            
            assert guess_count <= 10, "Too many guesses"
    
    with IOCapturing(generate_responses) as cap:
        run_path("number_guesser_solution.py")
        
    nums = extract_numbers(cap.get_stdout())
    assert nums[-1] == target
    print("Session:", cap.get_io())

if __name__ == "__main__":
    do_test(432)

            
    
    