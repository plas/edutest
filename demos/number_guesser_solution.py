first = 1
last = 999

while True:
    offer = (first + last) // 2
    print("My offer is %d" % offer)
    response = input("Is this correct (c) or your number is bigger (b) or smaller (s)? ")
    if response == "c":
        print("Yay!")
        break
    elif response == "b":
        first = offer+1
    elif response == "s":
        last = offer-1
