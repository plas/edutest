from textwrap import dedent
from edutest import ForbiddenIO, ForbiddenInputError,\
    ForbiddenOutputError, ForbiddenInput, IOCapturing, OutOfInputsError
from nose.tools import raises

SAMPLE_IO_PROGRAM = \
"""
a = int(input("Enter first integer: "))
b = int(input("Enter second integer: "))
print("Their sum is " + str(a+b))
print("Their ratio is " + str(a // b))
"""

def test_forbidden_io_without_io():
    with ForbiddenIO():
        exec("x = 34; y = x*x")


@raises(ForbiddenOutputError)
def test_forbidden_io_with_output():
    with ForbiddenIO():
        exec("print('blah')")

@raises(ZeroDivisionError)
def test_forbidden_io_with_runtime_exception():
    with ForbiddenIO():
        exec("3 / 0")

        

def test_forbidden_input_without_input():
    with ForbiddenInput():
        exec("print('blah')")
    
@raises(ForbiddenInputError)
def test_forbidden_input_with_input():
    with ForbiddenInput():
        exec(SAMPLE_IO_PROGRAM)

def test_io_capturing_without_io():
    with IOCapturing() as cap:
        exec("x = 3*4")
    
    assert cap.get_io() == ""
    assert cap.get_stdout() == ""
    assert cap.get_last_stdout() == ""
    assert cap.get_stderr() == ""
    assert cap.get_remaining_inputs() == []
    
    
def test_io_capturing_without_errors():
    with IOCapturing(["6", "3"]) as cap:
        exec(SAMPLE_IO_PROGRAM)
    
    assert cap.get_io() == dedent("""\
        Enter first integer: 6
        Enter second integer: 3
        Their sum is 9
        Their ratio is 2
        """)
    
    assert cap.get_stdout() == dedent("""\
        Enter first integer: Enter second integer: Their sum is 9
        Their ratio is 2
        """)
        
    assert cap.get_last_stdout() == dedent("""\
        Their sum is 9
        Their ratio is 2
        """)
            
    assert cap.get_stderr() == ""

    assert cap.get_remaining_inputs() == []

@raises(OutOfInputsError)
def test_io_capturing_with_too_few_inputs():    
    with IOCapturing(["6"]):
        exec(SAMPLE_IO_PROGRAM)

def test_io_capturing_with_too_many_inputs():    
    with IOCapturing(["6", "3", "4"]) as cap:
        exec(SAMPLE_IO_PROGRAM)

    assert cap.get_io() == dedent("""\
        Enter first integer: 6
        Enter second integer: 3
        Their sum is 9
        Their ratio is 2
        """)
    
    assert cap.get_remaining_inputs() == ["4"]

@raises(ZeroDivisionError)
def test_io_capturing_with_runtime_error():
    with IOCapturing(["6", "0"]):
        exec(SAMPLE_IO_PROGRAM)



def test_io_capturing_with_generator():
    
    def generate_blahs(cap):
        while "enter" in cap.get_last_stdout().lower():
            yield "blah"
        
    
    with IOCapturing(generate_blahs) as cap:
        exec(dedent("""\
        first_name = input("Enter first name: ")
        last_name = input("Enter last name: ")
        print("Your name is " + first_name + " " + last_name) 
        """))
    
    assert cap.get_io() == dedent("""\
        Enter first name: blah
        Enter last name: blah
        Your name is blah blah
        """)
    
    assert cap.get_remaining_inputs() == []
    
    
    