import runpy
import edutest
from nose.tools import raises 
import os.path

def test_explicit_script_fixing():
    path = "solution.py"
    edutest.set_target(path)
    assert edutest.find_target() == path

@raises(AssertionError)
def test_test_and_solution_without_env():
    os.environ[edutest.TARGET_SCRIPT_ENV_KEY] = ""
    check("test_and_solution", edutest.find_target, "solution.py")

def test_test_and_solution_with_env():
    os.environ[edutest.TARGET_SCRIPT_ENV_KEY] = "solution.py"
    check("test_and_solution", edutest.find_target, "solution.py")

@raises(AssertionError)
def test_several_candidates_finding():
    check("several_candidates", edutest.find_target, "solution1.py")


def test_matching_names_finding1():
    globs = run_path_in_samples("matching_names", "test_exercise1.py")
    assert os.path.basename(globs["script_name"]) == "exercise1.py"

def test_matching_names_finding2():
    globs = run_path_in_samples("matching_names", "exercise2_tests.py")
    assert os.path.basename(globs["script_name"]) == "exercise2.py"

def test_partial_submission1():
    globs = run_path_in_samples("partial_submission", "exercise1_tests.py")
    assert os.path.basename(globs["script_name"]) == "exercise1.py"

@raises(AssertionError)
def test_partial_submission2():
    globs = run_path_in_samples("partial_submission", "exercise2_tests.py")
    assert os.path.basename(globs["script_name"]) == "exercise2.py"


def check(sample, finding_function, expected_basename):
    assert os.path.basename(
        _find_testable_script_from(sample, finding_function)) ==  expected_basename

def run_path_in_samples(sample_name, script):
    init_dir = os.getcwd()
    
    try:
        os.chdir(os.path.join(os.path.dirname(__file__),
                              "script_finding_folders",
                              sample_name))
        return runpy.run_path(os.path.abspath(script))
    finally:
        os.chdir(init_dir)


def _find_testable_script_from(sample_name, finding_function):
    edutest.set_target(None)
    
    init_dir = os.getcwd()
    
    try:
        os.chdir(os.path.join(os.path.dirname(__file__),
                              "script_finding_folders",
                              sample_name))
        return finding_function()
    finally:
        os.chdir(init_dir)
    