from edutest import IOCapturing, load_locale
from edutest_locale_et import OUT_OF_INPUTS as OUT_OF_INPUTS_ET

def test_load_locale():
    load_locale("et")
    
    try:
        with IOCapturing():
            exec("x = input()")
    except Exception as e:
        assert str(e) == OUT_OF_INPUTS_ET
        
        

